# LOL TReplay
replay_lib.py is not included in this repository for proprietary reasons, if you want the built version of this repo, you should contact me.

## Replay folder structure

Downloaded replays will be located in the replays folder with the folder name: PLATFORM_GAMEID, eg. EUW1_4344479538
the replays folder is structured as follows:
```
replays/
 └── PLATFORM_GAMEID/
 |   └──────── chunks/
 │   └───── keyframes/
 |   └───── meta.json
 |   └───── Start_Replay.bat
 └── ...
 |
 .
 .
 .
```
> :warning: Do not change the name of the replay folder or any of it's files/subdirectories. The name is used during playback of the replay. The only file that is safe to rename is the Start_Replay.bat file, renaming any other file will result in the playback failing.


## API Key
Put your Riot Games API Key into api_key.txt.


## Downloading replays

1. Start replay_downloader.exe before the match starts(loading screen).
2. replay_downloader.exe will check for an active game every 30 seconds, once it detects a game it will begin downloading the replay. It is safe to leave the game at any time after it has started beginning the download.
3. replay_downloader.exe will exit once it has fully downloaded the game.

## Playing back replays

1. Start replay_server.exe.
2. Navigate to your replay folder of choice, and launch Start_Replay.bat, this will start League of Legends.

## Config.json

Set the game_path value to the path of your league of legends directory, make sure to use double slashes ie. `\\`, this is the path used when launching the replay from the Start_Replay.bat file. 

Platform and region should always match the below table. These 2 values decide which region/platform to look for the spectator data, eg. if you're spectating a game on NA, then you should set platform and region to `"NA1"` and `"NA"`. 

|Platform | Region|
|---------|-------|
|NA1 | NA|
|EUW1 | EUW|
|EUN1 | EUNE|
|TR1 | TR|
|LA1 | LAN|
|LA2 | LAS|
|OC1 | OCE|
|KR | KR|
|JP1 | JP|
|BR1 | BR|
|RU | RU|

## Notes

* If replay_downloader.exe hasnt started running before the in-game timer reads 8:00, then there is no guarantee that the replay will be fully recorded.

* To play replays downloaded on an older patch, set the game path to the league of legends directory that is running on that patch, and the replay will play without issues.
