import json
import urllib.request
import ssl
import time
import cassiopeia as cass
from cassiopeia.datastores.riotapi.common import APIRequestError
from datapipelines.common import NotFoundError
from replay_lib import ReplayDownloader, Replay


def log_file(msg):
    with open('errors.txt', 'w') as f:
        f.write(msg)

def query_live_game_data(config, downloader):
    try:
        with urllib.request.urlopen('https://localhost:2999/liveclientdata/allgamedata', context=ssl._create_unverified_context()) as url:
            data = json.loads(url.read().decode())
    except:
        print('No active game found... retrying in 30s...')
        time.sleep(30)
        query_live_game_data(config, downloader)
        return
    print('Active game found')
    name = data['allPlayers'][0]['summonerName']
    try:
        match = cass.get_current_match(cass.get_summoner(name=name, region=config['region']), region=config['region'])
        key = match.observer_key
        game_id = match.id
    except APIRequestError:
        log_file('Invalid API Key.')
        return
    except NotFoundError:
        log_file('NotFoundError')
        return

    replay = Replay(config['platform'], game_id, key)
    try:
        replay.download_replay(downloader)
    except:
        log_file('Failed to download replay, likely wrong platform in config.')
        return
    write_bat_file(config['platform'], game_id, key)
    

    
def write_bat_file(region, game_id, key):
    bat_file = f'''SETLOCAL enableextensions
cd ..
cd ..
FOR /F "delims=" %%A IN ('xidel -s config.json -e "$json() ! eval(x'{{.}}:=$json/{{.}}')[0]" --output-format^=cmd') DO %%A
set RADS_PATH=%game_path%
if exist %RADS_PATH%\Game (
    cd /d %RADS_PATH%\Config
    for /F "delims=" %%a in ('find "        locale: " LeagueClientSettings.yaml') do set "locale=%%a"
    for /F "tokens=2 delims=: " %%a in ("%locale%") do set "locale=%%a"

    SET GAME_PATH="%RADS_PATH%\Game"
    @cd /d %GAME_PATH%

    if exist "League of Legends.exe" (
        @start "" "League of Legends.exe" "8394" "LoLLauncher.exe" "" "spectator 127.0.0.1:3030 {key} {game_id} {region}" "-UseRads" "-Locale=%locale%"
    )
)'''
    with open(f'replays/{region}_{game_id}/Start_Replay.bat', 'w') as f:
        f.write(bat_file)


if __name__ == "__main__":
    try:
        with open('config.json', 'r') as f:
            config = json.load(f)

    except IOError:
        log_file('Could not find config.json, creating file...')
        with open('config.json', 'w') as f:
            json.dump({'region': 'NA', 'platform': 'NA1', 'game_path': 'C:\\Riot Games\\League of Legends'}, f)

    except json.JSONDecodeError:
        log_file('Invalid config file.. exiting')

    try:
        with open('api_key.txt', 'r') as f:
            api_key = f.read().strip()
            cass.set_riot_api_key(api_key)

    except IOError:
        log_file('No api_key.txt found, creating...')
        open('api_key.txt', 'w').close()
    
    downloader = ReplayDownloader()
    query_live_game_data(config, downloader)

